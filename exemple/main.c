#include <stdio.h>

#include "tag_checker.h"

int main(int argc, char* argv[]) {
    if(argc < 2) {
        return -1;
    }

    tager_TagChecker_t checker;

    tager_InitTagChecker(&checker);

    int ret = tager_OpenAndCheck(&checker, argv[1]);

    tager_ResetTager(&checker);

    printf("\nFile is good?: %s\n", ret == 0 ? "true" : "false");

    return 0;
}
