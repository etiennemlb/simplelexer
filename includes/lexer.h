#ifndef SIMPLE_LEXER_LEXER_H
#define SIMPLE_LEXER_LEXER_H

#include <stdint.h>
#include <unistd.h>

#include "fsm.h"

/**
 * @file
 * @brief This file define the necessary types and functions of a lexer. 
 * 
 */

/**
 * @brief This enumeration represents the return value for some of the function
 * which can be used to manipulate the lexer.
 * 
 */
typedef enum {
    LEXER_RET_ERROR    = -1, /**< Represents an error, -1 can be used too. */
    LEXER_RET_NEEDMORE = 1,  /**< Could not deduce a token. */
    LEXER_RET_GOT_TOKEN,     /**< Got a fresh new token. */
    LEXER_RET_BAD_SYNTAX     /**< Bad syntax in file. */
} lexer_RetVal_e;

/**
 * @brief This structure represents a token. It's filled by the function
 * lexer_GetNextToken().
 * The user may use the variable member len, data, and meaning.
 */
typedef struct
{
    fsm_ParsingUnit* data;     /**< Represents the string extracted from the file.
                                Malloced mem. */
    size_t           len_max_; /**< Size of data. */
    size_t           len;      /**< Size of the string data. */
    int              meaning;  /**< The meaning of the state. */
} lexer_Token_t;

/**
 * @brief The lexer struct is initialized with the function lexer_InitLexer().
 * The user may not use the members of the struct and should rather use the functions.
 * 
 */
typedef struct
{
    fsm_FSM_t* smf_; /**< The grammar used by the lexer. */
} lexer_SimpleLexer_t;

/**
 * @brief Used to initialize a lexer.
 * 
 * @param lexer A valid ptr to a lexer.
 * @param grammar A valid ptr to the grammar (fsm_FSM_t).
 * @return int -1 if error, else 0, errno set appropriately. 
 */
int lexer_InitLexer(lexer_SimpleLexer_t* lexer,
                    fsm_FSM_t*           grammar);

/**
 * @brief 
 * 
 * @param lexer A valid ptr to a lexer_SimpleLexer_t
 * @param token A valid ptr to a token. Will be filled with a token extracted 
 *              from data if the file is valid. If there is not enough data in data
 *              the lexer will ask for more (see lexer_RetVal_e).
 * @param data An array of fsm_ParsingUnit.
 * @param available The length of valid fsm_ParsingUnit in data.
 * @param consumed The amount of data consumed (in fsm_ParsingUnit).
 * @return lexer_RetVal_e -1 if error, else 0, errno set appropriately.
 */
lexer_RetVal_e lexer_GetNextToken(const lexer_SimpleLexer_t* lexer,
                                  lexer_Token_t*             token,
                                  const fsm_ParsingUnit*     data,
                                  size_t                     available,
                                  size_t*                    consumed);

/**
 * @brief Initialize a token (memset 0).
 * 
 * @param tok A valid ptr to a token.
 */
void lexer_InitToken(lexer_Token_t* tok);

/**
 * @brief Free the internal members of the token. Does not free tok.
 * 
 * @param tok A valid ptr to a token.
 */
void lexer_ResetToken(lexer_Token_t* tok);

#endif
