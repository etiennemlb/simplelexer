# SimpleLexer

Simple lexer is a crappy projet that aims to create tools to help deducing 
tokens out of a file.

This project commes with an exemple called html_tag_checker.

debug and utils are not part of the library (those two are redundant and should 
be factorised in a library of their own).

## To compile you might need:
cmake

gcc > 4 or clang

Its currently supported only on linux.

## Compile:

$ mkdir build && cd build

$ cmake -DCMAKE_BUILD_TYPE=[Release|Debug] ..

$ cmake --build . -- -j 16 # or $ make -j 16

## Generate the doc

$ cd docs

$ mkdir docs

$ doxygen Main

The generated doc start at "docs/html/index.html"

## Usage:

./html_tag_checker <path_to_file_to_check>
