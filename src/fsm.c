#include "fsm.h"

#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include "debug/debug.h"

int fsm_InitFSM(fsm_FSM_t* fsm,
                size_t     state_count) {
    if(fsm == NULL) {
        errno = EFAULT;
        return -1;
    }

    memset(fsm, 0, sizeof(fsm_FSM_t));

    if((fsm->states_ = malloc(sizeof(fsm_State_t__) * state_count)) == NULL) {
        return -1;
    }

    memset(fsm->states_, 0, (sizeof(fsm_State_t__) * state_count));

    fsm->state_count_max_ = state_count;

    return 0;
}

static void fsm_ResetState(const fsm_FSM_t* fsm,
                           fsm_State_t      state);

void fsm_ResetFSM(fsm_FSM_t* fsm) {
    if(fsm == NULL) {
        return;
    }

    for(size_t i = 0; i < fsm->state_count_max_; i++) {
        fsm_ResetState(fsm, i);
    }

    if(fsm->states_) {
        free(fsm->states_);
    }
}

int fsm_SetCurrentState(fsm_FSM_t* fsm,
                        size_t     state) {
    if(fsm == NULL) {
        errno = EFAULT;
        return -1;
    }

    if(state >= fsm->state_count_) {
        errno = EINVAL;
        return -1;
    }

    fsm->current_state_ = state;

    return 0;
}

static int fsm_IsEndState(const fsm_State_t__* state) {
    if(state == NULL) {
        errno = EFAULT;
        return -1;
    }

    if(state->len_ == FSM_FINAL_STATE) {
        return 1;
    }
    return 0;
}

static int fsm_StateCanFoward(const fsm_State_t__* state, fsm_State_t* next, fsm_ParsingUnit c) {
    if(state == NULL || next == NULL) {
        errno = EFAULT;
        return -1;
    }

    dbprintf("\tOn char '%c'\n", c);
    for(size_t i = 0; i < state->len_; ++i) {
        fsm_ParsingUnit a = state->checks_[i].a_;
        fsm_ParsingUnit b = state->checks_[i].b_;

        *next = state->next_state_lut_[i];

        switch(state->checks_[i].type_) {
            case FSM_CHECK_RANGE: /* [a-b]       */
                if(a <= c && c <= b) {
                    return FSM_RET_SUCCESS;
                }
                break;

            case FSM_CHECK_PRECISE: /* (a|b)       */
                if(a == c || b == c) {
                    return FSM_RET_SUCCESS;
                }
                break;

            case FSM_CHECK_ALL_BUT_RANGE: /* ^[a-b]      */
                if(!(a <= c && c <= b)) {
                    return FSM_RET_SUCCESS;
                }
                break;

            case FSM_CHECK_ALL_BUT: /* ^(a|b)      */
                if(!(a == c || b == c)) {
                    return FSM_RET_SUCCESS;
                }
                break;

            case FSM_CHECK_a_z: /* [a-z]       */
                if('a' <= c && c <= 'z') {
                    return FSM_RET_SUCCESS;
                }
                break;

            case FSM_CHECK_A_Z: /* [A-Z]       */
                if('A' <= c && c <= 'Z') {
                    return FSM_RET_SUCCESS;
                }
                break;

            case FSM_CHECK_a_Z: /* [a-zA-Z]    */
                if(('a' <= c && c <= 'z') ||
                   ('A' <= c && c <= 'Z')) {
                    return FSM_RET_SUCCESS;
                }
                break;

            case FSM_CHECK_0_9: /* [0-9]       */
                if('0' <= c && c <= '9') {
                    return FSM_RET_SUCCESS;
                }
                break;

            case FSM_CHECK_aZ09: /* [a-zA-Z0-9] */
                if(('a' <= c && c <= 'z') ||
                   ('A' <= c && c <= 'Z') ||
                   ('0' <= c && c <= '9')) {
                    return FSM_RET_SUCCESS;
                }
                break;

            case FSM_CHECK_INVISIBLE:
                dbprintf("Check inbi\n");
                if(c == '\b' || c == '\f' || c == '\n' || c == '\r' ||
                   c == '\t' || c == '\v' || c == ' ') {
                    return FSM_RET_SUCCESS;
                }
                break;

            default:
                dbprintf("\tcheck error");
                return -1;
        }
    }
    return FSM_RET_BADSYNTAX;
}

fsm_RetVal_e fsm_Forward(fsm_FSM_t*      fsm,
                         fsm_ParsingUnit c) {
    if(fsm == NULL) {
        errno = EFAULT;
        return -1;
    }

    fsm_State_t next;
    int         ret;

    dbprintf("fsm->current_state_: %ld\n", fsm->current_state_);

    ret = fsm_StateCanFoward(&fsm->states_[fsm->current_state_],
                             &next,
                             c);

    switch(ret) {
        case FSM_RET_BADSYNTAX:
            return FSM_RET_BADSYNTAX;

        case FSM_RET_SUCCESS: /* we can change state */
            fsm->current_state_ = next;

            ret = fsm_IsEndState(&fsm->states_[fsm->current_state_]);

            if(ret == 1) {
                return FSM_RET_ENDED;
            } else if(ret == -1) {
                return -1;
            }

            return FSM_RET_SUCCESS;

        default:
            return -1;
    }
}

fsm_RetVal_e fsm_FastForward(fsm_FSM_t*             fsm,
                             const fsm_ParsingUnit* str,
                             size_t                 available,
                             size_t*                consumed) {
    if(fsm == NULL || str == NULL || consumed == NULL) {
        errno = EFAULT;
        return -1;
    }

    for(*consumed = 0; *consumed < available; ++(*consumed)) {
        int ret = fsm_Forward(fsm, str[*consumed]);

        if(ret == FSM_RET_SUCCESS) {
            continue;
        }

        if(ret == FSM_RET_ENDED) {
            /* no backtrack */
            if(fsm->states_[fsm->current_state_].backtrack_ == 0) {
                ++(*consumed);
            }
        }
        return ret;
    }

    return FSM_RET_SUCCESS;
}

int fsm_GetCurrentState(const fsm_FSM_t* fsm, fsm_State_t* state) {
    if(fsm == NULL || state == NULL) {
        errno = EFAULT;
        return -1;
    }

    *state = fsm->current_state_;

    return 0;
}

int fsm_GetCurrentStateMeaning(const fsm_FSM_t* fsm,
                               int*             meaning) {
    if(fsm == NULL || meaning == NULL) {
        errno = EFAULT;
        return -1;
    }

    *meaning = fsm->states_[fsm->current_state_].meaning_;

    return 0;
}

int fsm_AddGuard(const fsm_FSM_t* fsm,
                 fsm_State_t      from,
                 fsm_ParsingUnit  a,
                 fsm_ParsingUnit  b,
                 fsm_State_t      to,
                 fsm_CheckType_e  type)

{
    if(fsm == NULL) {
        errno = EFAULT;
        return -1;
    }

    if(type == FSM_CHECK_ALL_BUT_RANGE || type == FSM_CHECK_RANGE) {
        if(a > b) {
            errno = EINVAL;
            return -1;
        }
    }

    if(from >= fsm->state_count_) {
        errno = EINVAL;
        return -1;
    }

    if(to >= fsm->state_count_) {
        errno = EINVAL;
        return -1;
    }

    if(fsm->states_[from].len_ == fsm->states_[from].len_max_) {
        errno = EPERM;
        return -1;
    }

    fsm->states_[from].checks_[fsm->states_[from].len_].a_    = a;
    fsm->states_[from].checks_[fsm->states_[from].len_].b_    = b;
    fsm->states_[from].checks_[fsm->states_[from].len_].type_ = type;

    fsm->states_[from].next_state_lut_[fsm->states_[from].len_] = to;

    ++fsm->states_[from].len_;

    return 0;
}

int fsm_InitState(fsm_FSM_t*    fsm,
                  fsm_State_t*  state,
                  fsm_Meaning_t meaning,
                  size_t        guard_count,
                  uint32_t      backtrack) {
    if(fsm == NULL || state == NULL) {
        errno = EFAULT;
        return -1;
    }

    if(guard_count != FSM_FINAL_STATE) {
        if((fsm->states_[fsm->state_count_].next_state_lut_ =
                malloc(sizeof(fsm_State_t) * guard_count)) == NULL) {
            return -1;
        }

        if((fsm->states_[fsm->state_count_].checks_ =
                malloc(sizeof(fsm_Check_t) * guard_count)) == NULL) {
            free(fsm->states_[fsm->state_count_].next_state_lut_);
            return -1;
        }
    }

    // guarenteed to be memsetted to 0
    /* fsm->states_[fsm->state_count_].len_ = 0; */
    fsm->states_[fsm->state_count_].len_max_   = guard_count;
    fsm->states_[fsm->state_count_].meaning_   = meaning;
    fsm->states_[fsm->state_count_].backtrack_ = backtrack;

    *state = fsm->state_count_++;

    return 0;
}

static void fsm_ResetState(const fsm_FSM_t* fsm,
                           fsm_State_t      state) {
    if(fsm == NULL) {
        return;
    }

    if(state >= fsm->state_count_max_) {
        return;
    }

    /* printf("On state %ld\n", state); */
    if(fsm->states_[state].checks_) {
        free(fsm->states_[state].checks_);
    }

    if(fsm->states_[state].next_state_lut_) {
        free(fsm->states_[state].next_state_lut_);
    }
}

int fsm_SetInitialState(fsm_FSM_t* fsm, fsm_State_t state) {
    if(fsm == NULL) {
        errno = EFAULT;
        return -1;
    }

    if(state >= fsm->state_count_) {
        errno = EINVAL;
        return -1;
    }

    fsm->default_ = state;

    return 0;
}

int fsm_GetInitialState(const fsm_FSM_t* fsm, fsm_State_t* state) {
    if(fsm == NULL || state == NULL) {
        errno = EFAULT;
        return -1;
    }

    *state = fsm->default_;

    return 0;
}
