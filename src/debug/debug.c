#include "debug.h"

#include <stdarg.h>
#include <stdio.h>


#ifndef NDEBUG

void dbprintf(const char* fmt, ...) {
    va_list list;
    va_start(list, fmt);

    vprintf(fmt, list);

    va_end(list);
}

#endif // DEBUG
