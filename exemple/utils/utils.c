#include "utils.h"

#include <fcntl.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

int OpenRead(const char* filename) {
    return open(filename, O_RDONLY | O_NDELAY);
}

const char* MemMapRead(const char* filename, size_t* filesize) {
    int   fd = OpenRead(filename);
    char* map;

    if(fd >= 0) {
        register off_t o = lseek(fd, 0, SEEK_END);
        if(o == 0 || (sizeof(off_t) != sizeof(size_t) && o > (off_t)(size_t)-1)) {
            close(fd);
            return NULL;
        }

        *filesize = (size_t)o;

        if(o > 0) {
            map = mmap(0, *filesize, PROT_READ, MAP_SHARED, fd, 0);
            if(map == (char*)-1) {
                map = NULL;
            }
        } else {
            map = NULL;
        }

        close(fd);
        return map;
    }
    return NULL;
}

int MemUnMap(const char* mapped, size_t maplen) {
    return maplen ? munmap((char*)mapped, maplen) : 0;
}

int IsInvisibleChar(char c) {
    return c == '\b' || c == '\f' || c == '\n' || c == '\r' ||
           c == '\t' || c == '\v' || c == ' ';
}
