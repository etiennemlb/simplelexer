#include "lexer.h"

#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include "debug/debug.h"

int lexer_InitLexer(lexer_SimpleLexer_t* lexer,
                    fsm_FSM_t*           grammar) {
    if(lexer == NULL || grammar == NULL) {
        errno = EFAULT;
        return -1;
    }

    lexer->smf_ = grammar;

    return 0;
}

lexer_RetVal_e lexer_GetNextToken(const lexer_SimpleLexer_t* lexer,
                                  lexer_Token_t*             token,
                                  const fsm_ParsingUnit*     data,
                                  size_t                     available,
                                  size_t*                    consumed) {
    if(lexer == NULL || token == NULL ||
       data == NULL || consumed == NULL) {
        errno = EFAULT;
        return -1;
    }

    fsm_RetVal_e ret;        /* ret val of the fsm */
    fsm_State_t  init_state; /* initial stte of the fsm */

    if((ret = fsm_FastForward(lexer->smf_, data, available, consumed)) < 0) {
        return -1;
    }

    dbprintf("Here3 consumed: %ld retval:%d\n", *consumed, ret);

    if(ret == FSM_RET_ENDED) {
        fsm_GetCurrentStateMeaning(lexer->smf_, &token->meaning);
        fsm_SetCurrentState(lexer->smf_, fsm_GetInitialState(lexer->smf_, &init_state));
    }

    if(token->len > 0) {
        // realloc maybe ?
        if((token->len_max_ - token->len) <= (*consumed + 1)) {
            token->len_max_ = (token->len_max_ + *consumed) * 2;
            if((token->data = realloc(token->data,
                                      token->len_max_)) == NULL) {
                return -1;
            }

            dbprintf(
                "Here3 lexer get token need more space, "
                "allocating %ld\n",
                token->len_max_);
        }
    } else {
        // no allocated mem
        token->len_max_ = *consumed * 2;
        if((token->data = malloc(sizeof(fsm_ParsingUnit) * token->len_max_)) == NULL) {
            return -1;
        }

        memset(token->data, 0, sizeof(fsm_ParsingUnit) * token->len_max_);

        dbprintf("Here3 lexer get token new token, allocating %ld\n", token->len_max_);
    }

    strncpy((char*)token->data + token->len, (const char*)data, *consumed);
    token->len += *consumed;

    switch(ret) {
        case FSM_RET_BADSYNTAX:
            return LEXER_RET_BAD_SYNTAX;
        case FSM_RET_ENDED:
            return LEXER_RET_GOT_TOKEN;
        case FSM_RET_SUCCESS:
            return LEXER_RET_NEEDMORE;
        default:
            return -1;
    }

    return ret;
}

void lexer_InitToken(lexer_Token_t* tok) {
    if(tok == NULL) {
        errno = EFAULT;
        return;
    }

    memset(tok, 0, sizeof(lexer_Token_t));
}

void lexer_ResetToken(lexer_Token_t* tok) {
    if(tok == NULL) {
        errno = EFAULT;
        return;
    }

    if(tok->data) {
        free(tok->data);
    }

    memset(tok, 0, sizeof(lexer_Token_t));
}
