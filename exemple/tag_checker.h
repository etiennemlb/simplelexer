#ifndef SIMPLE_LEXER_TAG_CHECKER_H
#define SIMPLE_LEXER_TAG_CHECKER_H

#include <lexer.h>

/**
 * @file
 * @brief This file define the necessary types and functions used to parse only the tags
 * of a html file. 
 * 
 */

/**
 * @brief The red color code for a Linux tty.
 * 
 */
#define TAG_CHECKER_RED "\x1B[31m"
/**
 * @brief The red color code for a Linux tty.
 * 
 */
#define TAG_CHECKER_GRN "\x1B[32m"
/**
 * @brief The green color code for a Linux tty.
 * 
 */
#define TAG_CHECKER_YEL "\x1B[33m"
/**
 * @brief The yellow color code for a Linux tty.
 * 
 */
#define TAG_CHECKER_BLU "\x1B[34m"
/**
 * @brief The blue color code for a Linux tty.
 * 
 */
#define TAG_CHECKER_MAG "\x1B[35m"
/**
 * @brief The cyan color code for a Linux tty.
 * 
 */
#define TAG_CHECKER_CYN "\x1B[36m"
/**
 * @brief The white color code for a Linux tty.
 * 
 */
#define TAG_CHECKER_WHT "\x1B[37m"
/**
 * @brief The reset code for colors for a Linux tty.
 * 
 */
#define TAG_CHECKER_RST "\x1B[0m"

/**
 * @brief Meanings of the state for the html grammar.
 * 
 */
typedef enum {
    TAG_OPEN = 0,   /**< e.i. \<xxx> */
    TAG_CLOSE,      /**< e.i. \</xxx> */
    TAG_OPEN_CLOSE, /**< e.i. \<osefoesef/> */
    TEXT,           /**< e.i. \<xxx>Text here</xxx> */
    NONE            /**< Incomplete token */
} HTMLStateMeaning_e;

/**
 * @brief This struct represents a html tag.
 * 
 */
typedef struct
{
    fsm_Meaning_t    meaning; /**< The meaning of the tag (see fsm_Meaning_t). */
    fsm_ParsingUnit* data;    /**< The cstring representing the tag (extracted for the file) */
} tager_Tag_t;

/**
 * @brief This struct represents the tag parser. It deduce using the output of the lexer
 * the validity of the arrangement of the tags.
 * 
 */
typedef struct
{
    fsm_FSM_t grammar_; /**< Finite sate machine representing the grammar. */

    ssize_t      max_stack_len_;    /**< The maximum size of the stack. */
    ssize_t      stack_ptr_;        /**< The current position for the stack. */
    tager_Tag_t* meaningful_stack_; /**< The array containing the stack. */
} tager_TagChecker_t;

/**
 * @brief Initialize a tager.
 * 
 * @param tager A valid tager_TagChecker_t ptr.
 * @return int -1 if errors, else 0. 
 */
int tager_InitTagChecker(tager_TagChecker_t* tager);

/**
 * @brief Open a file, parse it and stop if errors are found.
 * 
 * @param tager A valid tager_TagChecker_t ptr.
 * @param path The parth of the file to parse.
 * @return int -1 if errors or file does not match the grammar, else success 0. 
 *                  errno set appropriatly.
 */
int tager_OpenAndCheck(tager_TagChecker_t* tager, char* path);

/**
 * @brief Resets a tager freeing the internal members. Does not free the tager.
 * 
 * @param tager A valid tager ptr.
 * @return int -1 if errors, else 0. errno set appropriatly.
 */
int tager_ResetTager(tager_TagChecker_t* tager);

#endif
