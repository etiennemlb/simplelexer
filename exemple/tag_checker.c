#include "tag_checker.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "debug/debug.h"
#include "utils/utils.h"


static int tag_parser_BuildHTMLGrammar(tager_TagChecker_t* tager) {
    if(tager == NULL) {
        errno = EFAULT;
        return -1;
    }

    fsm_State_t states[30];

    if(fsm_InitFSM(&tager->grammar_, 32) < 0) {
        dbprintf("Could not create fsm");
        return -1;
    }

    fsm_FSM_t* fsm = &tager->grammar_;

    /* STATE INIT */

    /* Initial state */
    {
        fsm_InitState(fsm, &states[0], NONE, 2, FSM_NO_STATE_BACKTRACK);

        /* Tags */
        {
            fsm_InitState(fsm, &states[1], NONE, 2, FSM_NO_STATE_BACKTRACK);
            /* End tag */
            {
                fsm_InitState(fsm, &states[2], NONE, 1, FSM_NO_STATE_BACKTRACK);
                fsm_InitState(fsm, &states[3], NONE, 3, FSM_NO_STATE_BACKTRACK);
                fsm_InitState(fsm, &states[4], NONE, 2, FSM_NO_STATE_BACKTRACK);
                fsm_InitState(fsm, &states[5], TAG_CLOSE, FSM_FINAL_STATE, FSM_NO_STATE_BACKTRACK);
            }
            /* Opening tag */
            {
                fsm_InitState(fsm, &states[6], NONE, 4, FSM_NO_STATE_BACKTRACK);
                fsm_InitState(fsm, &states[7], NONE, 4, FSM_NO_STATE_BACKTRACK);
                fsm_InitState(fsm, &states[8], NONE, 5, FSM_NO_STATE_BACKTRACK);
                fsm_InitState(fsm, &states[9], NONE, 2, FSM_NO_STATE_BACKTRACK);
                fsm_InitState(fsm, &states[10], NONE, 2, FSM_NO_STATE_BACKTRACK);
                fsm_InitState(fsm, &states[11], NONE, 2, FSM_NO_STATE_BACKTRACK);
                fsm_InitState(fsm, &states[12], NONE, 2, FSM_NO_STATE_BACKTRACK);
                fsm_InitState(fsm, &states[13], NONE, 1, FSM_NO_STATE_BACKTRACK);
                fsm_InitState(fsm, &states[14], NONE, 1, FSM_NO_STATE_BACKTRACK);
                fsm_InitState(fsm, &states[15], TAG_OPEN_CLOSE, FSM_FINAL_STATE, FSM_NO_STATE_BACKTRACK);
                fsm_InitState(fsm, &states[16], TAG_OPEN, FSM_FINAL_STATE, FSM_NO_STATE_BACKTRACK);
            }
        }
        /* Text */
        {
            fsm_InitState(fsm, &states[17], NONE, 2, FSM_NO_STATE_BACKTRACK);
            fsm_InitState(fsm, &states[18], TEXT, FSM_FINAL_STATE, FSM_STATE_BACKTRACK);
        }
    }

    /* GUARDS */

    /* Initial state */
    {
        fsm_AddGuard(fsm, states[0], '<', 0, states[1], FSM_CHECK_PRECISE);
        fsm_AddGuard(fsm, states[0], '<', 0, states[17], FSM_CHECK_ALL_BUT);

        /* Tags */
        {
            fsm_AddGuard(fsm, states[1], '/', 0, states[2], FSM_CHECK_PRECISE);
            fsm_AddGuard(fsm, states[1], 0, 0, states[6], FSM_CHECK_aZ09);
            /* End tag */
            {
                fsm_AddGuard(fsm, states[2], 0, 0, states[3], FSM_CHECK_aZ09);

                fsm_AddGuard(fsm, states[3], 0, 0, states[3], FSM_CHECK_aZ09);
                fsm_AddGuard(fsm, states[3], 0, 0, states[4], FSM_CHECK_INVISIBLE);
                fsm_AddGuard(fsm, states[3], '>', 0, states[5], FSM_CHECK_PRECISE);

                fsm_AddGuard(fsm, states[4], '>', 0, states[5], FSM_CHECK_PRECISE);
            }
            /* Opening tag */
            {
                fsm_AddGuard(fsm, states[6], 0, 0, states[6], FSM_CHECK_aZ09); /* should be extended */
                fsm_AddGuard(fsm, states[6], 0, 0, states[7], FSM_CHECK_INVISIBLE);
                fsm_AddGuard(fsm, states[6], '/', 0, states[14], FSM_CHECK_PRECISE);
                fsm_AddGuard(fsm, states[6], '>', 0, states[16], FSM_CHECK_PRECISE);

                fsm_AddGuard(fsm, states[7], 0, 0, states[7], FSM_CHECK_INVISIBLE);
                fsm_AddGuard(fsm, states[7], 0, 0, states[8], FSM_CHECK_aZ09); /* should be extended */
                fsm_AddGuard(fsm, states[7], '/', 0, states[14], FSM_CHECK_PRECISE);
                fsm_AddGuard(fsm, states[7], '>', 0, states[16], FSM_CHECK_PRECISE);

                fsm_AddGuard(fsm, states[8], 0, 0, states[8], FSM_CHECK_aZ09);        /* should be extended */
                fsm_AddGuard(fsm, states[8], '_', '-', states[8], FSM_CHECK_PRECISE); /* should be extended */
                fsm_AddGuard(fsm, states[8], '=', 0, states[9], FSM_CHECK_PRECISE);
                fsm_AddGuard(fsm, states[8], 0, 0, states[11], FSM_CHECK_INVISIBLE);
                fsm_AddGuard(fsm, states[8], '/', 0, states[14], FSM_CHECK_PRECISE);
                fsm_AddGuard(fsm, states[8], '>', 0, states[16], FSM_CHECK_PRECISE);

                fsm_AddGuard(fsm, states[9], '"', 0, states[10], FSM_CHECK_PRECISE);
                fsm_AddGuard(fsm, states[9], 0, 0, states[12], FSM_CHECK_INVISIBLE);

                fsm_AddGuard(fsm, states[10], '"', '\\', states[10], FSM_CHECK_ALL_BUT);
                fsm_AddGuard(fsm, states[10], '"', 0, states[7], FSM_CHECK_PRECISE);
                fsm_AddGuard(fsm, states[10], '\\', 0, states[13], FSM_CHECK_PRECISE);

                fsm_AddGuard(fsm, states[11], 0, 0, states[11], FSM_CHECK_INVISIBLE);
                fsm_AddGuard(fsm, states[11], '=', 0, states[9], FSM_CHECK_PRECISE);

                fsm_AddGuard(fsm, states[12], 0, 0, states[12], FSM_CHECK_INVISIBLE);
                fsm_AddGuard(fsm, states[12], '"', 0, states[10], FSM_CHECK_PRECISE);

                fsm_AddGuard(fsm, states[13], 0, 0, states[10], FSM_CHECK_ALL_BUT);

                fsm_AddGuard(fsm, states[14], '>', 0, states[14], FSM_CHECK_PRECISE);
            }
        }
        /* Text */
        {
            fsm_AddGuard(fsm, states[17], '<', 0, states[17], FSM_CHECK_ALL_BUT);
            fsm_AddGuard(fsm, states[17], '<', 0, states[18], FSM_CHECK_PRECISE);
        }
    }

    // set default state and starting state
    fsm_SetInitialState(fsm, states[0]);
    fsm_SetCurrentState(fsm, states[0]);

    return 0;
}

int tager_InitTagChecker(tager_TagChecker_t* tager) {
    if(tager == NULL) {
        errno = EFAULT;
        return -1;
    }

    if(tag_parser_BuildHTMLGrammar(tager) < 0) {
        return -1;
    }

    tager->max_stack_len_ = 50; /* should be way moore than enough */

    if((tager->meaningful_stack_ = malloc(sizeof(*tager->meaningful_stack_) * tager->max_stack_len_)) == NULL) {
        return -1;
    }

    tager->stack_ptr_ = -1;

    return 0;
}

static int tager_PushStack(tager_TagChecker_t* tager,
                           tager_Tag_t*        tag) {
    if(tager == NULL || tag == NULL) {
        errno = EFAULT;
        return -1;
    }

    if(tager->stack_ptr_ >= (tager->max_stack_len_ - 1)) {
        errno = EPERM;
        return -1;
    }

    memcpy(&tager->meaningful_stack_[++(tager->stack_ptr_)], tag, sizeof(*tag));
    return 0;
}

static int tager_PeekStack(tager_TagChecker_t* tager,
                           tager_Tag_t*        tag) {
    if(tager == NULL || tag == NULL) {
        errno = EFAULT;
        return -1;
    }

    if(tager->stack_ptr_ < 0) {
        errno = EPERM;
        return -1;
    }

    memcpy(tag, &tager->meaningful_stack_[tager->stack_ptr_], sizeof(*tag));
    return 0;
}

static int tager_PopStack(tager_TagChecker_t* tager) {
    if(tager == NULL) {
        errno = EFAULT;
        return -1;
    }

    if(tager->stack_ptr_ < 0) {
        errno = EPERM;
        return -1;
    }

    free(tager->meaningful_stack_[tager->stack_ptr_].data);

    --tager->stack_ptr_;

    return 0;
}

static int CmpTagType(const fsm_ParsingUnit* open, const fsm_ParsingUnit* close) {
    if(open == NULL || close == NULL) {
        errno = EFAULT;
        return -1;
    }

    ssize_t i = 0;

    open += 1;  /* <aaa> to aaa> */
    close += 2; /* </aaa> to aaa> */

    while(close[i] != '\0' && open[i] != '\0') {
        int close_is_invisible = IsInvisibleChar(close[i]);
        int open_is_invisible  = IsInvisibleChar(open[i]);

        /* dirty and slow */
        // if we reached the end of a tag
        if(close_is_invisible || close[i] == '>' ||
           open_is_invisible || open[i] == '>') {
            // if both end correctly
            if((close_is_invisible || close[i] == '>') &&
               (open_is_invisible || open[i] == '>')) {
                return 0;
            }

            return 1;
        }

        if(open[i] != close[i]) {
            return 1;
        }

        ++i;
    }

    return 0;
}

int tager_OpenAndCheck(tager_TagChecker_t* tager, char* path) {
    if(tager == NULL) {
        errno = EFAULT;
        return -1;
    }

    lexer_SimpleLexer_t lexer;               /* this lexer will use the grammar used to 
                                * detect invalid tag in a html file */
    size_t              file_size;           /* size of the html file */
    const uint8_t*      data;                /* a ptr to the mapped file */
    lexer_Token_t       tok;                 /* this token will be constructed by the 
                                * lexer and reseted */
    size_t              bytes_read = 0;      /* bytes read */
    size_t              bytes_left;          /* bytes used */
    size_t              bytes_consumed = 0;  /* bytes consumed by the lexer */
    int                 success        = -1; /* ret val*/
    lexer_RetVal_e      ret            = -1; /* ret val of the lexer */

    if(lexer_InitLexer(&lexer, &tager->grammar_) < 0) {
        return -1;
    }

    if((data = (uint8_t*)MemMapRead(path, &file_size)) == NULL) {
        return -1;
    }

    bytes_left = file_size;

    lexer_InitToken(&tok);

    while(1) {
        bytes_read += bytes_consumed;
        bytes_left = file_size - bytes_read;

        if(bytes_left == 0) {
            success = -1;

            if(tager->stack_ptr_ == -1) {
                success = 0; /* All good file is valid */
            }

            if(ret == LEXER_RET_NEEDMORE) {
                printf(TAG_CHECKER_RED "%s" TAG_CHECKER_RST, tok.data);
                success = -1;
            }

            goto cleanup;
        }

        lexer_ResetToken(&tok);

        ret = lexer_GetNextToken(&lexer,
                                 &tok,
                                 data + bytes_read,
                                 bytes_left,
                                 &bytes_consumed);

        switch(ret) {
            case LEXER_RET_NEEDMORE:
                dbprintf("Need more\n");
                continue;

            case LEXER_RET_GOT_TOKEN:
                switch(tok.meaning) {
                    case TAG_OPEN: {
                        dbprintf("Token type is: TAG_OPEN\n");
                        printf(TAG_CHECKER_GRN "%s" TAG_CHECKER_RST, tok.data);

                        // move data to an other struct
                        tager_Tag_t tag = {tok.meaning, tok.data};
                        // reinit the token
                        lexer_InitToken(&tok);
                        if(tager_PushStack(tager, &tag) < 0) {
                            success = -1;
                            goto cleanup;
                        }
                        break;
                    }
                    case TAG_CLOSE: {
                        dbprintf("Token type is: TAG_CLOSE\n");

                        tager_Tag_t tag;

                        if(tager_PeekStack(tager, &tag) < 0) {
                            success = -1;
                            goto cleanup;
                        }

                        // similar type
                        if(tag.meaning != TAG_OPEN) {
                            success = -1;
                            goto cleanup;
                        }

                        // now check for similar tag
                        if(CmpTagType(tag.data, tok.data) != 0) {
                            printf(TAG_CHECKER_RED "%s" TAG_CHECKER_RST, tok.data);
                            success = -1;
                            goto cleanup;
                        }

                        printf(TAG_CHECKER_WHT "%s" TAG_CHECKER_RST, tok.data);

                        // free the tag struct
                        if(tager_PopStack(tager) < 0) {
                            success = -1;
                            goto cleanup;
                        }
                        break;
                    }
                    case TAG_OPEN_CLOSE:
                        dbprintf("Token type is: TAG_CLOSE\n");
                        printf(TAG_CHECKER_BLU "%s" TAG_CHECKER_RST, tok.data);
                        break;

                    case TEXT:
                        dbprintf("Token type is: TEXT\n");
                        printf(TAG_CHECKER_YEL "%s" TAG_CHECKER_RST, tok.data);
                        break;

                    default:
                        dbprintf("Token type is: useless\n");
                        printf(TAG_CHECKER_BLU "%s" TAG_CHECKER_RST, tok.data);
                        break;
                }
                break;

            case LEXER_RET_BAD_SYNTAX:
                printf(TAG_CHECKER_RED "%s" TAG_CHECKER_RST, tok.data);
                __attribute__((fallthrough));
            case LEXER_RET_ERROR:
            default:
                success = -1;
                goto cleanup;
        }
    }

cleanup:

    MemUnMap((const char*)data, file_size);
    lexer_ResetToken(&tok);

    return success; /* non empty stack, invalid file */
}

int tager_ResetTager(tager_TagChecker_t* tager) {
    if(tager == NULL) {
        errno = EFAULT;
        return -1;
    }

    fsm_ResetFSM(&tager->grammar_);

    // reset the tag list
    tager_Tag_t tag;
    while(tager_PeekStack(tager, &tag) > -1) {
        tager_PopStack(tager);
    }

    if(tager->meaningful_stack_ != NULL) {
        free(tager->meaningful_stack_);
    }

    return 0;
}
