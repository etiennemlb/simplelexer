#ifndef SIMPLE_LEXER_CONFIG_H
#define SIMPLE_LEXER_CONFIG_H

#ifdef NDEBUG
static inline void dbprintf(const char* fmt, ...) {
    (void)fmt;
}
#else
/**
 * @brief Printf with a condition for debug
 * 
 * @param fmt format string
 * @param ... args
 */
void dbprintf(const char* fmt, ...);
#endif // ! NDEBUG

#endif
