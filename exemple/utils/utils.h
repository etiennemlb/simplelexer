#ifndef FANTAS_UTILS_H
#define FANTAS_UTILS_H

#include <sys/types.h>

/**
 * @brief Open a file for read
 * 
 * @param filename 
 * @return int 
 */
int OpenRead(const char* filename);

/**
 * @brief Map a memory area to a file
 * 
 * @param filename 
 * @param filesize 
 * @return const char* 
 */
const char* MemMapRead(const char* filename, size_t* filesize);

/**
 * @brief Unmap a maped memory area
 * 
 * @param mapped 
 * @param maplen mapped mem size
 * @return int < 0 if error
 */
int MemUnMap(const char* mapped, size_t maplen);

/**
 * @brief http://crasseux.com/books/ctutorial/Special-characters.html
 * 
 * @param c 
 * @return int 1 if is invisible
 */
int IsInvisibleChar(char c);

#endif //  FANTAS_UTILS_H
