#ifndef SIMPLE_LEXER_FSM_H
#define SIMPLE_LEXER_FSM_H

#include <stdint.h>
#include <unistd.h>

/**
 * @file
 * @brief This file define the necessary types and functions modeling a fsm. 
 * 
 */

/**
 * @brief Use this macro do define the number of guards of a state with no guard.
 * To be used in conjonction with fsm_InitState()
 */
#define FSM_FINAL_STATE 0

/**
 * @brief Use this if the fsm needs NOT to backtrack if it is in this state.
 * To be used in conjonction with fsm_InitState()
 */
#define FSM_NO_STATE_BACKTRACK 0

/**
 * @brief Use this if the fsm needs to backtrack if it is in this state.
 * To be used in conjonction with fsm_InitState()
 */
#define FSM_STATE_BACKTRACK 1

/**
 * @brief Type used to represent a state.
 * 
 */
typedef size_t fsm_State_t;

/**
 * @brief Type used to represent a meaning.
 * 
 */
typedef int fsm_Meaning_t;

/**
 * @brief Type used to represent the working granularity of the fsm.
 * 
 */
typedef unsigned char fsm_ParsingUnit;

/**
 * @brief This enumeration define the different ways the fsm can use to chech if a character
 * is valide and can allow a change of state.
 * 
 */
typedef enum {
    FSM_CHECK_RANGE,         /**< [a-b]       */
    FSM_CHECK_ALL_BUT_RANGE, /**< ^[a-b]      */
    FSM_CHECK_PRECISE,       /**< (a|b)       */
    FSM_CHECK_ALL_BUT,       /**< ^(a|b)      */
    FSM_CHECK_a_z,           /**< [a-z]       */
    FSM_CHECK_A_Z,           /**< [A-Z]       */
    FSM_CHECK_a_Z,           /**< [a-zA-Z]    */
    FSM_CHECK_0_9,           /**< [0-9]       */
    FSM_CHECK_aZ09,          /**< [a-zA-Z0-9] */
    FSM_CHECK_INVISIBLE,     /**< (\\b| |\\f|\\v|\\n|\\r|\\t)  */
} fsm_CheckType_e;

/**
 * @brief Represent the checks available for the fsm to change state.
 * Should not be used by the user.
 * 
 */
typedef struct
{
    fsm_CheckType_e type_; /**< Type of the check. */
    fsm_ParsingUnit a_;    /**< Lower bound of the range. Might not be used depending on type_. */
    fsm_ParsingUnit b_;    /**< Higher bound of the range.Might not be used depending on type_.  */
} fsm_Check_t;

/**
 * @brief This enumeration represents the return value for some of the function
 * which can be used to manipulate the fsm.
 * 
 */
typedef enum {
    FSM_RET_SUCCESS,   /**< The fsm successfuly went forward in the graph. */
    FSM_RET_ENDED,     /**< The fsm deduced a valid token. */
    FSM_RET_BADSYNTAX, /**< The fsm cant match the text to the grammar. */
} fsm_RetVal_e;

/**
 * @brief This structure represent the state as seen internaly by the fsm.
 * This should not be manipulated by the user.
 * 
 */
typedef struct
{
    fsm_Check_t* checks_;         /**< Array of checks. */
    fsm_State_t* next_state_lut_; /**< Lut of the next state for a given guard idx. */
    size_t       len_;            /**< Current guard count. */
    size_t       len_max_;        /**< Max number of guard. */

    uint32_t      backtrack_; /**< Sometime the only way to extract a token is by 
                            consuming some more char than what's actualy part of
                            the (to be) extracted token. In this case, it's 
                            possible to go back one char. */
    fsm_Meaning_t meaning_;   /**< It's possible to add a meaning to a state. When 
                               on a peculiar state, it's possible to retrieve it. */
} fsm_State_t__;

/**
 * @brief This structure represent the fsm. It should be allocated and then initialized
 * using fsm_InitFSM. The content of the struct should not be used by the user.
 * 
 */
typedef struct
{
    size_t current_state_; /**< Current state of the fsm */

    size_t         state_count_max_; /**< Max number of state */
    size_t         state_count_;     /**< Current state count */
    fsm_State_t__* states_;          /**< Malloced state array */
    fsm_State_t    default_;         /**< default state */
} fsm_FSM_t;

/**
 * @brief This function initialize a finite state machine.
 * 
 * @param fsm a valid ptr to a fsm.
 * @param state_count the number of state the fsm should hold.
 * @return int -1 if and error occured, else 0, errno set appropriately.
 */
int fsm_InitFSM(fsm_FSM_t* fsm,
                size_t     state_count);

/**
 * @brief Free the internal variable and memory of the fsm.
 * Does not free the fsm struct itslef.
 * 
 * @param fsm A valid fsm ptr.
 */
void fsm_ResetFSM(fsm_FSM_t* fsm);

/**
 * @brief Define the current state of the fsm.
 * 
 * @param fsm A valid fsm ptr.
 * @param state A valid staet (created using fsm_InitState).
 * @return int -1 if error, else 0, errno set appropriately.
 */
int fsm_SetCurrentState(fsm_FSM_t* fsm,
                        size_t     state);

/**
 * @brief Try to change state with a given char.
 * 
 * @param fsm A valid fsm ptr.
 * @param c The char.
 * @return fsm_RetVal_e -1 if erron, errno set appropriatly.
 */
fsm_RetVal_e fsm_Forward(fsm_FSM_t*      fsm,
                         fsm_ParsingUnit c);

/**
 * @brief Try to change state given a list of character.
 * 
 * @param fsm A valid fsm ptr.
 * @param str A valid character array.
 * @param available The size of the array (or the valid character it contains).
 * @param consumed A valid ptr to a size. Will be filled with the quantity of 
 *                 character read by the fsm.
 * @return fsm_RetVal_e -1 if error, errno set appropriatly.
 */
fsm_RetVal_e fsm_FastForward(fsm_FSM_t*             fsm,
                             const fsm_ParsingUnit* str,
                             size_t                 available,
                             size_t*                consumed);

/**
 * @brief Allows the user to get the current state of the fsm.
 * 
 * @param fsm A valid fsm ptr.
 * @param state A valid ptr to a fsm_State_t. Will be filled with the current state.
 * @return int -1 if error, else 0. errno set appropriatly.
 */
int fsm_GetCurrentState(const fsm_FSM_t* fsm,
                        fsm_State_t*     state);

/**
 * @brief Allows the user to get the meaning of the current state of the fsm.
 * 
 * @param fsm A valid fsm ptr.
 * @param meaning A valid ptr to a fsm_Meaning_t. Will be filled with the current meaning.
 * @return int -1 if error, else 0. errno set appropriatly.
 */
int fsm_GetCurrentStateMeaning(const fsm_FSM_t* fsm,
                               fsm_Meaning_t*   meaning);

/**
 * @brief Adds a guard to a state to allow transition from a state to an other.
 * 
 * @param fsm A valid fsm ptr.
 * @param state A valid state created with fsm_InitState.
 *              Represent the source of the transition.
 * @param a Represent the lower bound of a range. Check fsm_CheckType_e for more details.
 *          Depending on the type used might not be used andcan be set to 0.
 * @param b Represent the higher bound of a range. Check fsm_CheckType_e for more details.
 *          Depending on the type used might not be used andcan be set to 0.
 * @param to A valid state created with fsm_InitState.
 *           Represent the destination of the transition.
 * @param type The type of check to allow the transition.
 * @return int -1 if error, else 0. errno set appropriatly.
 */
int fsm_AddGuard(const fsm_FSM_t* fsm,
                 fsm_State_t      state,
                 fsm_ParsingUnit  a,
                 fsm_ParsingUnit  b,
                 fsm_State_t      to,
                 fsm_CheckType_e  type);

/**
 * @brief Adds a state to the fsm.
 * 
 * @param fsm A valid fsm ptr.
 * @param state A valid fsm_State_t ptr. Will be filled with the id of the newly created state.
 * @param meaning The meaning associated with the state.
 * @param guard_count The number of guard the state needs.
 * @param backtrack Does the fsm needs to backtrack one unit if it gets in this state. 
 * @return int -1 if error, else 0. errno set appropriatly.
 */
int fsm_InitState(fsm_FSM_t*    fsm,
                  fsm_State_t*  state,
                  fsm_Meaning_t meaning,
                  size_t        guard_count,
                  uint32_t      backtrack);

/**
 * @brief Sets the initial state of the fsm.
 * 
 * @param fsm A valid fsm ptr.
 * @param state A valid fsm_State_t.
 * @return int -1 if error, else 0. errno set appropriatly.
 */
int fsm_SetInitialState(fsm_FSM_t* fsm, fsm_State_t state);

/**
 * @brief Gets the initial state of the fsm.
 * 
 * @param fsm A valid fsm ptr.
 * @param state A valid ptr to a fsm_State_t. Will be filed with e initial state.
 * @return int -1 if error, else 0. errno set appropriatly.
 */
int fsm_GetInitialState(const fsm_FSM_t* fsm, fsm_State_t* state);

#endif
